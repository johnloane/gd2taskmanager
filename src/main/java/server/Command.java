package server;

import core.TaskManager;

public interface Command
{
    public String generateResponse(String [] components, TaskManager taskList);
}
