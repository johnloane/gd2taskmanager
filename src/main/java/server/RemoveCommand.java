package server;

import core.Task;
import core.TaskManager;
import core.TaskManagerServiceDetails;

public class RemoveCommand implements Command
{
    @Override
    public String generateResponse(String[] components, TaskManager taskList)
    {
        //remove%%OOPCA6
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_REMOVE)
        {
            String taskName = components[TaskManagerServiceDetails.TASK_NAME_INDEX];
            Task taskToBeRemoved = new Task(taskName);
            boolean removed = taskList.remove(taskToBeRemoved);

            if(removed)
            {
                response = TaskManagerServiceDetails.SUCCESSFUL_REMOVE;
            }
            else
            {
                response = TaskManagerServiceDetails.FAILED_REMOVE;
            }
        }
        return response;
    }
}
