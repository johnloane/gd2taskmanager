package server;

import core.Task;
import core.TaskManager;
import core.TaskManagerServiceDetails;

import java.util.Date;
import java.util.Set;

public class ViewAllCommand implements Command
{
    @Override
    public String generateResponse(String[] components, TaskManager taskList)
    {
        //viewAll
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_VIEWALL)
        {
            Set<Task> tasks = taskList.getAllTasks();
            response = TaskManagerServiceDetails.flattenTaskSet(tasks);
            if(response == null)
            {
                response = "Dummy Task%%Dummy Owner%%" + new Date().getTime();
            }
        }
        return response;
    }
}
