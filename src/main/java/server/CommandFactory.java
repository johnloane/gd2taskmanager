package server;

import core.TaskManagerServiceDetails;

public class CommandFactory
{
    public static Command createCommand(String requestCommand)
    {
        Command c = null;
        switch(requestCommand)
        {
            case TaskManagerServiceDetails.ADD_COMMAND:
                c = new AddCommand();
                break;
            case TaskManagerServiceDetails.REMOVE_COMMAND:
                c = new RemoveCommand();
                break;
            case TaskManagerServiceDetails.VIEW_COMMAND:
                c = new ViewAllCommand();
                break;
            case TaskManagerServiceDetails.EXIT_COMMAND:
                c = new ExitCommand();
                break;
        }
        return c;
    }
}
