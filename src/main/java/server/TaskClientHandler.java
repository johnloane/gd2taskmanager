package server;

import core.Task;
import core.TaskManager;
import core.TaskManagerServiceDetails;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;

public class TaskClientHandler implements Runnable
{
    private Socket clientSocket;
    private TaskManager taskList;

    public TaskClientHandler(Socket clientSocket, TaskManager taskList)
    {
        this.clientSocket = clientSocket;
        this.taskList = taskList;
    }

    @Override
    public void run()
    {
        try
        {
            //Step 1: Open the lines of communication
            //An example of the decorator pattern
            Scanner clientInput = new Scanner(clientSocket.getInputStream());
            PrintWriter clientOutput = new PrintWriter(clientSocket.getOutputStream(), true);

            //Step 2: Set up repeated exchanges
            //Look at what the client sends and respond appropriately
            boolean sessionActive = true;
            while(sessionActive)
            {
                //Protocol logic
                String request = clientInput.nextLine();

                //Break up the request into components
                //add%%OOPCA6%%John%%1234567
                //exit
                //viewAll
                //remove%%OOPCA6
                String [] components = request.split(TaskManagerServiceDetails.BREAKING_CHARACTERS);
                String response = null;

                Command c = CommandFactory.createCommand(components[0]);
                if(c != null)
                {
                    response = c.generateResponse(components, taskList);

                    if(response != null)
                    {
                        clientOutput.println(response);
                        if(c instanceof ExitCommand)
                        {
                            sessionActive = false;
                        }
                    }
                }

            }
            clientSocket.close();
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }
}
