package server;

import core.TaskManager;
import core.TaskManagerServiceDetails;

public class ExitCommand implements Command
{

    @Override
    public String generateResponse(String[] components, TaskManager taskList)
    {
        String response = null;
        if(components.length == 1)
        {
            response = TaskManagerServiceDetails.SIGN_OFF;
        }
        return response;
    }
}
