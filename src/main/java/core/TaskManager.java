package core;

import java.util.HashSet;
import java.util.Set;

public class TaskManager
{
    private final Set<Task> tasks;

    public TaskManager()
    {
        this.tasks = new HashSet<>();
    }

    public boolean add(Task t)
    {
        synchronized (tasks)
        {
            return tasks.add(t);
        }
    }

    public boolean remove(Task t)
    {
        synchronized (tasks)
        {
            return tasks.remove(t);
        }
    }

    public Set<Task> getAllTasks()
    {
        Set<Task> duplicateSet = new HashSet();
        synchronized (tasks)
        {
            for (Task t : tasks)
            {
                duplicateSet.add(t);
            }
        }
        return duplicateSet;
    }
}
