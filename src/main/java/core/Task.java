package core;

import java.util.Date;
import java.util.Objects;

public class Task
{
    private String taskName;
    private String taskOwner;
    private Date deadline;

    public Task(String taskName, String taskOwner, Date deadline)
    {
        this.taskName = taskName;
        this.taskOwner = taskOwner;
        this.deadline = deadline;
    }

    public Task(String taskName)
    {
        this.taskName = taskName;
    }

    public String getTaskName()
    {
        return taskName;
    }

    public String getTaskOwner()
    {
        return taskOwner;
    }

    public Date getDeadline()
    {
        return deadline;
    }

    @Override
    public String toString()
    {
        return "Task{" +
                "taskName='" + taskName + '\'' +
                ", taskOwner='" + taskOwner + '\'' +
                ", deadline=" + deadline +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(taskName, task.taskName);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(taskName);
    }

    public String format()
    {
        return taskName +TaskManagerServiceDetails.BREAKING_CHARACTERS+ taskOwner + TaskManagerServiceDetails.BREAKING_CHARACTERS + deadline.getTime();
    }
}
